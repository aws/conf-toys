' Capture all page changes (jumps included)
Sub OnSlideShowPageChange(ByVal Wn As SlideShowWindow)
     Wn.Presentation.Slides(1).NotesPage.Shapes(2).TextFrame.TextRange.InsertAfter ("[" & Date & " " & Time & "] " & "Slide Changed to: " & Wn.View.CurrentShowPosition & vbCrLf)
End Sub